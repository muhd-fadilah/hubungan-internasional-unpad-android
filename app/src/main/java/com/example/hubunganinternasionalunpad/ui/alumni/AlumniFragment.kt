package com.example.hubunganinternasionalunpad.ui.alumni

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import androidx.fragment.app.Fragment
import com.example.hubunganinternasionalunpad.R

class AlumniFragment: Fragment() {
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.alumni_fragment, container, false)
        val webView: WebView = root.findViewById(R.id.web_alumni)
        webView.loadUrl("file:///android_asset/html/alumni.html")
        return root
    }
}