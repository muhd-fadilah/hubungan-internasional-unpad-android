package com.example.hubunganinternasionalunpad.ui.praktikumterintegrasi

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.hubunganinternasionalunpad.R
import com.github.barteksc.pdfviewer.PDFView
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener
import com.github.barteksc.pdfviewer.util.FitPolicy
import kotlinx.android.synthetic.main.praktikum_terintegrasi_fragment.*

class PraktikumTerintegrasiFragment: Fragment(), OnPageChangeListener {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.praktikum_terintegrasi_fragment, container, false)
        val pdfPageNumber: TextView = root.findViewById(R.id.text_nomor_praktikum_terintegrasi)
        pdfPageNumber.text = getString(R.string.nomor_praktikum_terintegrasi, 1)
        val pdfView: PDFView = root.findViewById(R.id.pdf_praktikum_terintegrasi)
        pdfView.fromAsset("pdf/praktikum_terintegrasi.pdf")
            .swipeHorizontal(true)
            .pageSnap(true)
            .autoSpacing(true)
            .pageFling(true)
            .onPageChange(this)
            .pageFitPolicy(FitPolicy.BOTH)
            .enableAntialiasing(true)
            .load()
        return root
    }

    override fun onPageChanged(page: Int, pageCount: Int) {
        val pageNumber = page + 1
        text_nomor_praktikum_terintegrasi.text = getString(R.string.nomor_praktikum_terintegrasi, pageNumber)
    }
}