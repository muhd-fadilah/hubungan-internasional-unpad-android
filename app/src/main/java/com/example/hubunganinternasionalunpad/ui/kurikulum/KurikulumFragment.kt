package com.example.hubunganinternasionalunpad.ui.kurikulum

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.hubunganinternasionalunpad.R
import com.github.barteksc.pdfviewer.PDFView
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener
import com.github.barteksc.pdfviewer.util.FitPolicy
import kotlinx.android.synthetic.main.kurikulum_fragment.*

class KurikulumFragment: Fragment(), OnPageChangeListener {
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.kurikulum_fragment, container, false)
        val pdfPageNumber: TextView = root.findViewById(R.id.text_nomor_kurikulum)
        pdfPageNumber.text = getString(R.string.nomor_kurikulum, 1)
        val pdfView: PDFView = root.findViewById(R.id.pdf_kurikulum)
        pdfView.fromAsset("pdf/kurikulum.pdf")
                .swipeHorizontal(true)
                .pageSnap(true)
                .autoSpacing(true)
                .pageFling(true)
                .onPageChange(this)
                .pageFitPolicy(FitPolicy.BOTH)
                .enableAntialiasing(true)
                .load()
        return root
    }

    override fun onPageChanged(page: Int, pageCount: Int) {
        val pageNumber = page + 1
        text_nomor_kurikulum.text = getString(R.string.nomor_kurikulum, pageNumber)
    }
}