package com.example.hubunganinternasionalunpad.ui.kompetensilulusan

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import androidx.fragment.app.Fragment
import com.example.hubunganinternasionalunpad.R

class KompetensiLulusanFragment: Fragment() {
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.kompetensi_lulusan_fragment, container, false)
        val webView: WebView = root.findViewById(R.id.web_kompetensi_lulusan)
        webView.loadUrl("file:///android_asset/html/kompetensi_lulusan.html")
        return root
    }
}