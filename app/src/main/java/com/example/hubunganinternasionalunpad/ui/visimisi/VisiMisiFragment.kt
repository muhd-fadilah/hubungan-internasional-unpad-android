package com.example.hubunganinternasionalunpad.ui.visimisi

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import androidx.fragment.app.Fragment
import com.example.hubunganinternasionalunpad.R

class VisiMisiFragment: Fragment() {
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.visi_misi_fragment, container, false)
        val webView: WebView = root.findViewById(R.id.web_visi_misi)
        webView.loadUrl("file:///android_asset/html/visi_misi.html")
        return root
    }
}