package com.example.hubunganinternasionalunpad.ui.prospekkerja

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import androidx.fragment.app.Fragment
import com.example.hubunganinternasionalunpad.R

class ProspekKerjaFragment: Fragment() {
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.prospek_kerja_fragment, container, false)
        val webView: WebView = root.findViewById(R.id.web_prospek_kerja)
        webView.loadUrl("file:///android_asset/html/prospek_kerja.html")
        return root
    }
}