package com.example.hubunganinternasionalunpad.ui.stafpengajar

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import androidx.fragment.app.Fragment
import com.example.hubunganinternasionalunpad.R

class StafPengajarFragment: Fragment() {
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.staf_pengajar_fragment, container, false)
        val webView: WebView = root.findViewById(R.id.web_staf_pengajar)
        webView.loadUrl("file:///android_asset/html/staf_pengajar.html")
        return root
    }
}