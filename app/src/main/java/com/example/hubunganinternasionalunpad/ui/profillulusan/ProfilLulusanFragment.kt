package com.example.hubunganinternasionalunpad.ui.profillulusan

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import androidx.fragment.app.Fragment
import com.example.hubunganinternasionalunpad.R

class ProfilLulusanFragment: Fragment() {
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.profil_lulusan_fragment, container, false)
        val webView: WebView = root.findViewById(R.id.web_profil_lulusan)
        webView.loadUrl("file:///android_asset/html/profil_lulusan.html")
        return root
    }
}