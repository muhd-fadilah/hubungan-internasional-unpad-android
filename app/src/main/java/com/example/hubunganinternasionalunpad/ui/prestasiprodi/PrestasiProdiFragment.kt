package com.example.hubunganinternasionalunpad.ui.prestasiprodi

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import androidx.fragment.app.Fragment
import com.example.hubunganinternasionalunpad.R

class PrestasiProdiFragment: Fragment() {
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.prestasi_prodi_fragment, container, false)
        val webView: WebView = root.findViewById(R.id.web_prestasi_prodi)
        webView.loadUrl("file:///android_asset/html/prestasi_prodi.html")
        return root
    }
}